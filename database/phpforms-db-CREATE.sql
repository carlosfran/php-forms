-- MySQL Workbench Synchronization
-- Generated: 2021-03-12 11:52
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Carlos Fran

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `phpforms` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `phpforms`.`Formulario` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `titulo` VARCHAR(45) NOT NULL,
  `acao` VARCHAR(45) NULL DEFAULT NULL,
  `metodo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `phpforms`.`Pergunta` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `tipo` VARCHAR(45) NULL DEFAULT NULL,
  `Formulario_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Pergunta_Formulario_idx` (`Formulario_id` ASC),
  CONSTRAINT `fk_Pergunta_Formulario`
    FOREIGN KEY (`Formulario_id`)
    REFERENCES `phpforms`.`Formulario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `phpforms`.`Alternativa` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `texto` VARCHAR(45) NULL DEFAULT NULL,
  `valor` VARCHAR(45) NULL DEFAULT NULL,
  `tipo` VARCHAR(45) NULL DEFAULT NULL,
  `Pergunta_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Alternativa_Pergunta1_idx` (`Pergunta_id` ASC),
  CONSTRAINT `fk_Alternativa_Pergunta1`
    FOREIGN KEY (`Pergunta_id`)
    REFERENCES `phpforms`.`Pergunta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `phpforms`.`Resposta` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `resposta` VARCHAR(250) NOT NULL,
  `Pergunta_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Resposta_Pergunta1_idx` (`Pergunta_id` ASC),
  CONSTRAINT `fk_Resposta_Pergunta1`
    FOREIGN KEY (`Pergunta_id`)
    REFERENCES `phpforms`.`Pergunta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
