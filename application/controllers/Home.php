<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('GenericModel', 'model');
	}

	public function index()
	{
		$forms = $this->model->listar('Formulario');
		$this->load->view('lista-formularios', array('forms' => $forms));
	}

	public function form($id)
	{
		$form = $this->model->listar('Formulario', $id);
		$perguntas = $this->model->get_perguntas($id);
		
		$alternativas = array();
		foreach ($perguntas as $p) {
			$alternativas[$p['id']] = $this->model
				->get_alternativas($p['id']);
		}
		// print_r($form);

		$this->load->view('form', 
			array('form' => $form,
					'perguntas' => $perguntas,
					'alternativas' => $alternativas));
	}

	public function processa($form_id){
		var_dump($_POST);
		
		$respostas = array();

		# Gerando um id aqui mas
		# seria interessante ter algo do usuario logado.
		$objDateTime = new DateTime('NOW');
		$user_id = $form_id.$objDateTime->format('YmdHis');

		# print_r($_POST);
		
		foreach ($_POST as $key => $value) {
			$id = substr($key, 9);

			$resp = array( 'Pergunta_id' => $id,
							'resposta' => $value,
							'user_id' => $user_id);

			array_push($respostas, $resp);
		}
		$ret = $this->model->save_respostas($respostas);
		if($ret)
			echo 'Gravado com sucesso!';
		else
			echo 'Erro ao processar respostas!';
	}
}




